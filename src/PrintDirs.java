import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PrintDirs {
    private int depth;
    private List<String> result = new ArrayList<>();

    public PrintDirs(int depth) {
        this.depth = depth;
    }

    public List<String> showDirs(File path) {
        result.clear();
        showDirs(path, 0);
        return result;
    }

    public void showDirs(File path, int currentDepth) {
        if (!path.isDirectory() || !path.canRead() || currentDepth > depth) return;

        String separators = "";
        for (int i = 0; i < currentDepth; i++) {
            separators += "----";
        }

        result.add(separators + path.getName());

        Arrays.stream(path.list()).
                forEach(s -> showDirs(new File(path.getAbsolutePath() + '/' + s), currentDepth + 1));

    }
}
