import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        PrintDirs pd = new PrintDirs(2);
        File file = new File("/home/****/");
        List<String> result = pd.showDirs(file);

        for (String t: result) {
            System.out.println(t);
        }
    }
}
